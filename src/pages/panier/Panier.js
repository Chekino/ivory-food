import React from "react";

import "./panier.css";
import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import {
  MDBBtn,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBCardText,
  MDBCol,
  MDBContainer,
  MDBIcon,
  MDBInput,
  MDBRow,
  MDBTypography,
  MDBTooltip,
} from "mdb-react-ui-kit";
import {
  addToPanier,
  clearPanier,
  decreasePanier,
  getTotals,
  removeFromPanier,
} from "../../redux/reducers/panierSlice";

const Panier = () => {
  const paniers = useSelector((state) => state.panier);
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(getTotals());
  }, [paniers, dispatch]);

  const handleRemoveFromCart = (panier) => {
    dispatch(removeFromPanier(panier));
  };
  const handleDecreasePanier = (panier) => {
    dispatch(decreasePanier(panier));
  };
  const handleIncreasePanier = (panier) => {
    dispatch(addToPanier(panier));
  };
  const handleclearPanier = () => {
    dispatch(clearPanier());
  };

  return (
    <div className="panier-container">
      <MDBTypography tag="h1" className="fw-bold mb-0 text-black">
        Votre Panier
      </MDBTypography>
      {paniers.panier.length === 0 ? (
        <div className="panier-empty">
          <p>Votre panier est actuellement vide</p>
          <div className="start-shopping">
            <Link to="/menu" className="btn-retour-menu">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                fill="currentColor"
                className="bi bi-arrow-left"
                viewBox="0 0 16 16"
              >
                <path
                  fillRule="evenodd"
                  d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"
                />
              </svg>
              <span>Commencer vos achats</span>
            </Link>
          </div>
        </div>
      ) : (
        <div>
          <section className="h-100 h-custom">
            <MDBContainer className="py-5 h-100">
              <MDBRow className="justify-content-center align-items-center h-100">
                <MDBCol size="12">
                  <MDBCard
                    className="card-registration card-registration-2"
                    style={{ borderRadius: "15px" }}
                  >
                    <MDBCardBody className="p-0">
                      <MDBRow className="g-0">
                        <MDBCol lg="8">
                          <div className="p-5">
                            <div className="d-flex justify-content-between align-items-center mb-5">
                              <MDBTypography
                                tag="h3"
                                className="fw-bold mb-0 text-black"
                              >
                                Mes Selections
                              </MDBTypography>
                            </div>
                            <hr className="my-4" />
                            {paniers.panier?.map((panier) => (
                              <MDBRow className="mb-4 d-flex justify-content-between align-items-center">
                                <MDBCol md="2" lg="2" xl="2">
                                  <MDBCardImage
                                    src={panier.image}
                                    fluid
                                    className="rounded-3"
                                    alt="Cotton T-shirt"
                                  />
                                </MDBCol>
                                <MDBCol md="3" lg="3" xl="3">
                                  <MDBTypography
                                    tag="h6"
                                    className="text-black"
                                  >
                                    {panier.name}
                                  </MDBTypography>
                                  <MDBTypography
                                    tag="h6"
                                    className="text-muted mb-0"
                                  >
                                    {panier.price} FCFA
                                  </MDBTypography>
                                </MDBCol>
                                <MDBCol
                                  md="3"
                                  lg="3"
                                  xl="3"
                                  className="d-flex align-items-center"
                                >
                                  <button
                                    className="btn-moins"
                                    onClick={() => handleDecreasePanier(panier)}
                                  >
                                    <MDBIcon fas icon="minus" />
                                  </button>

                                  <input
                                    type="number"
                                    min="0"
                                    value={panier.panierQuantity}
                                    className="input-panier col-6"
                                  />

                                  <button
                                    className="btn-plus"
                                    onClick={() => handleIncreasePanier(panier)}
                                  >
                                    <MDBIcon icon="plus" />
                                  </button>
                                </MDBCol>
                                <MDBCol className="text-end">
                                  <MDBTypography tag="h6" className="mb-0">
                                    {panier.price * panier.panierQuantity} FCFA
                                  </MDBTypography>
                                </MDBCol>
                                <MDBCol
                                  md="1"
                                  lg="1"
                                  xl="1"
                                  className="text-end"
                                >
                                  <a
                                    href="#!"
                                    className="text-muted"
                                    onClick={() => handleRemoveFromCart(panier)}
                                  >
                                    <MDBIcon icon="times" />
                                  </a>
                                </MDBCol>
                              </MDBRow>
                            ))}
                            <button
                              className="btn-supprimer btn-fixed-width"
                              onClick={() => handleclearPanier()}
                            >
                              <MDBIcon icon="trash" />
                              Tout supprimé
                            </button>

                            <hr className="my-4" />
                            <div className="pt-5">
                              <MDBTypography tag="h6" className="mb-0">
                                <MDBCardText
                                  tag="a"
                                  href="#!"
                                  className="text-body"
                                >
                                  <MDBIcon icon="long-arrow-alt-left me-2" />
                                  <Link to={"/menu"}>Continuer vos achats</Link>
                                </MDBCardText>
                              </MDBTypography>
                            </div>
                          </div>
                        </MDBCol>
                        <MDBCol lg="4" className="bg-grey">
                          <div className="p-5">
                            <MDBTypography
                              tag="h3"
                              className="fw-bold mb-5 mt-2 pt-1"
                            >
                              Summary
                            </MDBTypography>

                            <hr className="my-4" />

                            <MDBTypography
                              tag="h5"
                              className="text-uppercase mb-3"
                            >
                              Give code
                            </MDBTypography>

                            <div className="mb-5">
                              <MDBInput size="lg" label="Enter your code" />
                            </div>

                            <hr className="my-4" />

                            <div className="d-flex justify-content-between mb-5">
                              <MDBTypography
                                tag="h5"
                                className="text-uppercase"
                              >
                                Total price
                              </MDBTypography>
                              <MDBTypography tag="h5">
                                {paniers.panierTotalAmount} FCFA
                              </MDBTypography>
                            </div>
                            {auth._id ? (
                              <MDBBtn color="dark" block size="lg">
                                Paiement
                              </MDBBtn>
                            ) : (
                              <MDBBtn
                                color="danger"
                                block
                                size="lg"
                                onClick={() => navigate("/authentification")}
                              >
                                connectez-vous
                              </MDBBtn>
                            )}
                          </div>
                        </MDBCol>
                      </MDBRow>
                    </MDBCardBody>
                  </MDBCard>
                </MDBCol>
              </MDBRow>
            </MDBContainer>
          </section>
        </div>
      )}
    </div>
  );
};

export default Panier;
