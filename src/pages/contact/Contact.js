import React from "react";

import image1 from "../../assets/customer-service-man.svg";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { add } from "../../redux/reducers/formSlice";

const Contact = () => {
  const dispatch = useDispatch();
  const [form, setForm] = useState({
    id: "",
    name: "",
    number: "",
    message: "",
  });

  function handleChange(e) {
    e.preventDefault();
    setForm({ ...form, [e.target.name]: e.target.value });
  }

  function submit(e) {
    e.preventDefault();
    dispatch(add({ ...form, id: Date.now() }));
  }

  return (
    <div>
      <div className="hero-section">
        <div className="container accueil-texte  py-5">
          <h1 className="text-center gros-titre text-white font-weight-bold  ">
            Contactez-Nous
          </h1>
          <div className="petite-ligne text-center">
            Si vous avez des questions, des commentaires ou si vous souhaitez
            simplement en savoir plus sur notre restaurant et notre cuisine,
            n'hésitez pas à nous contacter. Notre équipe dévouée est prête à
            répondre à toutes vos demandes.
          </div>
        </div>
      </div>
      <div className="py-5">
        <section>
          <div className="container">
            <div className="row">
              <div className="col-12 col-lg-6 mb-5">
                <div className="col-7">
                  <img src={image1} alt="" />
                </div>
              </div>
              <div className="col-12 col-lg-6">
                <form className="container ">
                  <div className="row">
                    <div className="col-md-6">
                      <div className="mb-3">
                        <label
                          htmlFor="nom"
                          className="form-label text-warning petite-ligne"
                        >
                          Nom du client
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          id="nom"
                          name="name"
                          onChange={handleChange}
                        />
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="mb-3">
                        <label
                          htmlFor="infos"
                          className="form-label text-warning petite-ligne"
                        >
                          Numero de telephone{" "}
                        </label>
                        <input
                          type="number"
                          className="form-control"
                          id="infos"
                          name="number"
                          onChange={handleChange}
                        />
                      </div>
                    </div>
                  </div>

                  <div className="mb-3">
                    <label
                      htmlFor="prix"
                      className="form-label text-warning petite-ligne"
                    >
                      Message :
                    </label>
                    <textarea
                      type="text"
                      className="form-control"
                      id="message"
                      name="message"
                      rows="10"
                      cols="50"
                      placeholder="Entrez votre message"
                      required
                      onChange={handleChange}
                    />
                  </div>

                  <div className="mb-3">
                    <button
                      type="submit"
                      className="btn btn-warning"
                      onClick={submit}
                    >
                      Envoyé le formulaire
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
        <section className="contact-form-sectioon"></section>
      </div>
    </div>
  );
};

export default Contact;
