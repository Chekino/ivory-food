import React, { useState } from "react";
import { MenuList } from "../../data/MenuList";
import "./menu.css";
import {
  MDBCard,
  MDBCardImage,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
} from "mdb-react-ui-kit";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";

import { Card, Button } from "react-bootstrap";
import { addToPanier } from "../../redux/reducers/panierSlice";
import Slider from "react-slick";

const Menu = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [selectedCategory, setSelectedCategory] = useState(null);
  const categories = [...new Set(MenuList.map((item) => item.category))]; // Extract unique categories
  const handleCategoryFilter = (category) => {
    setSelectedCategory(category);
  };
  const filteredMenu = selectedCategory
    ? MenuList.filter((item) => item.category === selectedCategory)
    : MenuList;

  const handleAddTopanier = (item) => {
    dispatch(addToPanier(item));
    navigate("/panier");
  };

  return (
    <>
      <div>
        <div className="hero-section ">
          <div className="container accueil-texte py-5 ">
            <h1 className="text-center gros-titre text-white font-weight-bold  ">
              Plat au Menu
            </h1>
            <div className="petite-ligne text-center">
              Notre menu à été soigneusement élaboré pour vous offrir une
              experience gastronomique authentique inoubliable pour vos papilles
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="categorie">CATEGORIES DES PLATS</div>
      </div>
      <div className="container text-center">
        {categories.map((category) => (
          <button
            className="btn btn-warning my-2 mx-2 "
            key={category}
            onClick={() => handleCategoryFilter(category)}
          >
            {category}
            <span className="coloos">
              ({MenuList.filter((item) => item.category === category).length})
            </span>
          </button>
        ))}
      </div>
      <div className="container  ">
        {categories.map((category) => (
          <div key={category}>
            <h4 className="text-center">
              {category} (
              {MenuList.filter((item) => item.category === category).length})
            </h4>

            <div className="row ">
              {filteredMenu
                .filter((item) => item.category === category)
                .map((item) => (
                  <MDBCard
                    key={item.id}
                    className="col-11 col-md-5 col-lg-3 m-3"
                  >
                    <MDBCardImage
                      className="img-menu"
                      position=""
                      alt="..."
                      src={item.image}
                    />
                    <MDBCardBody>
                      <b> {item.name}</b>
                    </MDBCardBody>

                    <div className="row">
                      <MDBCardText className="col-8">
                        {item.price} CFA
                      </MDBCardText>
                      <Button
                        variant="warning"
                        onClick={() => handleAddTopanier(item)}
                        className="col-4 "
                        style={{ height: "50px" }}
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          class="bi bi-cart-plus"
                          viewBox="0 0 16 16"
                        >
                          <path d="M9 5.5a.5.5 0 0 0-1 0V7H6.5a.5.5 0 0 0 0 1H8v1.5a.5.5 0 0 0 1 0V8h1.5a.5.5 0 0 0 0-1H9V5.5z" />
                          <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                        </svg>
                      </Button>
                    </div>
                  </MDBCard>
                ))}
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

export default Menu;
