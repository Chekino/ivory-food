import React from "react";
import "./accueil.css";
import avisclient from "./pexels-christina-morillo-1181391.jpg";
import avisclient2 from "./pexels-creation-hill-1681010.jpg";
import avisclient3 from "./pexels-emmy-e-2381069.jpg";
import imgafric from "./table-vue-dessus-pleine-delicieuses-compositions-alimentaires.jpg";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars, faCartArrowDown } from "@fortawesome/free-solid-svg-icons";
import { faCreditCard } from "@fortawesome/free-regular-svg-icons";
import { Container, Row, Col, Card } from "react-bootstrap";

const Accueil = () => {
  return (
    <>
      <section className="hero-section ">
        <div className="container">
          <div className="row">
            <div className="accueil-texte col-12  pt-5 mb-lg-0 text-center">
              <h1 className="gros-titre font-weight-bold mb-3">
                Bienvenue a IvOrY FooDs , un lieu d'exception où la cuisine
                raffinée rencontre l'accueil chaleureux.
              </h1>
              <div className="petite-ligne mb-3">
                Notre établissement est né de la passion pour la gastronomie et
                le désir de créer une expérience inoubliable pour nos clients.
                Découvrez donc une expérience culinaire unique et mémorable.
              </div>
              <div className="cta-btns mb-3">
                <Link className="btn btn-warning me-2" to="/menu">
                  Voir Menu
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div className="bg-section2-accueil">
        <section className="second-section">
          <div className="container">
            <div className="row">
              <div className="imgafric col-12 col-lg-6 mt-4 mb-4">
                <img src={imgafric} alt="" />
              </div>
              <div className=" col-12 col-lg-6 mt-4 mb-4 petite-ligne">
                <h1 className="gros-titre2  font-weight-bold ">
                  Découvrez une cuisine d'exception
                </h1>
                Notre menu soigneusement élaboré offre une variété de plats
                succulents qui raviront vos papilles. Des spécialités locales
                revisitées aux créations audacieuses, chaque assiette est
                préparée avec des ingrédients frais et de qualité. Nous
                travaillons en étroite collaboration avec des producteurs locaux
                pour vous offrir une expérience gastronomique authentique.
              </div>
            </div>
          </div>
        </section>
      </div>
      <div>
        <section>
          <div className="container">
            <div className="container">
              <div className="my-5 ">
                <div className="row d-flex justify-content-center gros-titre2">
                  Pour commandez un plat...
                </div>
                <div className="row d-flex justify-content-center big-color">
                  C'est très simple !
                </div>
              </div>
            </div>
            <div className="container">
              <div className="row  flex-nowrap overflow-auto">
                <div className="col-md-4">
                  <div className="text-center mb-3">
                    <FontAwesomeIcon
                      icon={faBars}
                      size="2x"
                      style={{ color: "#df9f16", height: "50px" }}
                    />
                  </div>
                  <div className="text-center gros-titre3 mb-2">
                    Consultez le menu
                  </div>
                  <div className="text-center">
                    Cliquez sur le bouton ou l'onglet menu
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="text-center mb-3">
                    <FontAwesomeIcon
                      icon={faCartArrowDown}
                      size="2x"
                      style={{ color: "#df9f16", height: "50px" }}
                    />
                  </div>
                  <div className="text-center gros-titre3 mb-2">
                    Choisissez ce dont vous avez envie
                  </div>
                  <div className="text-center">
                    Cherchez par catégorie le ou les plats que vous voulez et
                    ajoutez-les au panier
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="text-center mb-3">
                    <FontAwesomeIcon
                      icon={faCreditCard}
                      size="2x"
                      style={{ color: "#df9f16", height: "50px" }}
                    />
                  </div>
                  <div className="text-center gros-titre3 mb-2">
                    Commandez à emporter ou sur place
                  </div>
                  <div className="text-center">
                    Nous vous tiendrons au courant du déroulement de votre
                    commande.
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section>
          <section
            style={{ color: "#000", backgroundColor: "#fed7aa" }}
            className="m-5"
          >
            <Container className="py-5">
              <Row className="d-flex justify-content-center">
                <Col md={10} xl={8} className="text-center">
                  <h3 className="fw-bold mb-4">Retours clients</h3>
                  <p className="mb-4 pb-2 mb-md-5 pb-md-0">
                    Chez IvoryFood, nous nous efforçons de vous offrir une
                    expérience culinaire exceptionnelle. Votre opinion compte
                    énormément pour nous !
                  </p>
                </Col>
              </Row>

              <Row className="text-center">
                <Col md={4} className="mb-4 mb-md-0">
                  <Card>
                    <Card.Body className="py-4 mt-2">
                      <div className="d-flex justify-content-center mb-4">
                        <img
                          src={avisclient}
                          className="rounded-circle shadow-1-strong"
                          width="100"
                          height="100"
                          alt="Teresa May"
                        />
                      </div>
                      <h5 className="font-weight-bold">Alain Poda</h5>
                      <h6 className="font-weight-bold my-3">
                        Une expérience gastronomique incroyable
                      </h6>
                      <ul className="list-unstyled d-flex justify-content-center">
                        <li>
                          <i className="fas fa-star fa-sm text-info text-warning"></i>
                        </li>
                        <li>
                          <i className="fas fa-star fa-sm text-info text-warning"></i>
                        </li>
                        <li>
                          <i className="fas fa-star fa-sm text-info text-warning"></i>
                        </li>
                        <li>
                          <i className="fas fa-star fa-sm text-info text-warning"></i>
                        </li>
                        <li>
                          <i className="fas fa-star-half-alt fa-sm text-info text-warning"></i>
                        </li>
                      </ul>
                      <p className="mb-2">
                        <i className="fas fa-quote-left pe-2"></i>J'ai récemment
                        découvert IvoryFood, et je dois dire que c'est la
                        meilleure expérience de commande de nourriture en ligne
                        que j'ai jamais eue ! Le site web est convivial, avec
                        une large sélection de plats délicieux à choisir. J'ai
                        commandé le plat de pâtes aux fruits de mer, et la
                        qualité était exceptionnelle.
                      </p>
                    </Card.Body>
                  </Card>
                </Col>
                <Col md={4} className="mb-4 mb-md-0">
                  <Card>
                    <Card.Body className="py-4 mt-2">
                      <div className="d-flex justify-content-center mb-4">
                        <img
                          src={avisclient2}
                          className="rounded-circle shadow-1-strong"
                          width="100"
                          height="100"
                          alt="Teresa May"
                        />
                      </div>
                      <h5 className="font-weight-bold">Ouattara Mohamed</h5>
                      <h6 className="font-weight-bold my-3">
                        Excellent service client et choix varié
                      </h6>
                      <ul className="list-unstyled d-flex justify-content-center">
                        <li>
                          <i className="fas fa-star fa-sm text-info text-warning"></i>
                        </li>
                        <li>
                          <i className="fas fa-star fa-sm text-info text-warning"></i>
                        </li>
                        <li>
                          <i className="fas fa-star fa-sm text-info text-warning"></i>
                        </li>
                        <li>
                          <i className="fas fa-star fa-sm text-info text-warning"></i>
                        </li>
                        <li>
                          <i className="fas fa-star-half-alt fa-sm text-info text-warning"></i>
                        </li>
                      </ul>
                      <p className="mb-2">
                        <i className="fas fa-quote-left pe-2"></i> IvoryFood
                        offre une expérience de commande de nourriture en ligne
                        vraiment impressionnante. J'ai été ravi par le choix
                        varié de plats, des entrées aux desserts. J'ai opté pour
                        leur pizza spéciale, et elle était délicieuse. De plus,
                        le service client était exceptionnel. Ils ont répondu
                        rapidement à mes questions et m'ont aidé à personnaliser
                        ma commande.
                      </p>
                    </Card.Body>
                  </Card>
                </Col>
                <Col md={4} className="mb-4 mb-md-0">
                  <Card className="testi-card">
                    <Card.Body className="py-4 mt-2">
                      <div className="d-flex justify-content-center mb-4">
                        <img
                          src={avisclient3}
                          className="rounded-circle shadow-1-strong"
                          width="100"
                          height="100"
                          alt="Teresa May"
                        />
                      </div>
                      <h5 className="font-weight-bold">Josephine Dacan</h5>
                      <h6 className="font-weight-bold my-3">
                        Délicieux et pratique
                      </h6>
                      <ul className="list-unstyled d-flex justify-content-center">
                        <li>
                          <i className="fas fa-star fa-sm text-info text-warning"></i>
                        </li>
                        <li>
                          <i className="fas fa-star fa-sm text-info text-warning"></i>
                        </li>
                        <li>
                          <i className="fas fa-star fa-sm text-info text-warning"></i>
                        </li>
                        <li>
                          <i className="fas fa-star fa-sm text-info text-warning"></i>
                        </li>
                        <li>
                          <i className="fas fa-star-half-alt fa-sm text-info text-warning"></i>
                        </li>
                      </ul>
                      <p className="mb-2">
                        <i className="fas fa-quote-left pe-2"></i>IvoryFood est
                        devenu mon restaurant en ligne préféré pour commander de
                        délicieux repas sans tracas. J'ai utilisé leur site web
                        pour commander à plusieurs reprises, et à chaque fois,
                        la nourriture est incroyablement savoureuse. Leur site
                        est facile à naviguer, ce qui facilite le choix de plats
                        parmi leur vaste menu.
                      </p>
                    </Card.Body>
                  </Card>
                </Col>
              </Row>
            </Container>
          </section>
        </section>
      </div>
    </>
  );
};

export default Accueil;
