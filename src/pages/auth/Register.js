import React from "react";
import { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import {
  MDBBtn,
  MDBContainer,
  MDBCard,
  MDBCardBody,
  MDBCol,
  MDBRow,
  MDBInput,
  MDBCheckbox,
} from "mdb-react-ui-kit";
import { add, register } from "../../redux/reducers/authSlice";

function Register() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const auth = useSelector((state) => state.auth);
  console.log(auth);

  const [user, setUser] = useState({
    name: "",
    email: "",
    password: "",
  });

  useEffect(() => {
    if (auth._id) {
      navigate("/panier");
    }
  }, [auth._id, navigate]);

  const handleSubmit = (e) => {
    e.preventDefault();

    console.log(user);
    dispatch(register({ ...user, id: Date.now() }));
  };
  return (
    <MDBContainer className="p-3 my-5 d-flex flex-column w-50">
      <MDBInput
        wrapperClass="mb-4"
        placeholder="Nom Complet"
        id="name"
        type="name"
        onChange={(e) => setUser({ ...user, name: e.target.value })}
      />

      <MDBInput
        wrapperClass="mb-4"
        placeholder="Adresse E-mail"
        id="email"
        type="email"
        onChange={(e) => setUser({ ...user, email: e.target.value })}
      />
      <MDBInput
        wrapperClass="mb-4"
        placeholder="Mot de passe"
        id="password"
        type="password"
        onChange={(e) => setUser({ ...user, password: e.target.value })}
      />
      {auth.registerStatus === "rejected" ? (
        <p className="text-danger">{auth.registerError}</p>
      ) : null}
      <button className="btn-login  btn-warning mb-4" onClick={handleSubmit}>
        {auth.registerStatus === "pending" ? "Inscription..." : "S'inscrire"}
      </button>

      <div className="text-center">
        <Link to="/authentification">Se connecter</Link>
      </div>
    </MDBContainer>
  );
}

export default Register;
