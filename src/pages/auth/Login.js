import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { MDBContainer, MDBInput, MDBCheckbox, MDBBtn } from "mdb-react-ui-kit";
import { login } from "../../redux/reducers/authSlice";
import "./style.css";

function Login() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);

  useEffect(() => {
    if (auth._id) {
      navigate("/panier");
    }
  }, [auth._id, navigate]);

  const [user, setUser] = useState({
    email: "",
    password: "",
  });
  const handleSubmit = (e) => {
    e.preventDefault();

    dispatch(login({ ...user, id: Date.now() }));
    console.log(user);
  };

  return (
    <MDBContainer className="p-3 my-5 d-flex flex-column w-50">
      <MDBInput
        wrapperClass="mb-4"
        placeholder="Adresse E-mail"
        id="email"
        type="email"
        onChange={(e) => setUser({ ...user, email: e.target.value })}
      />
      <MDBInput
        wrapperClass="mb-4"
        placeholder="Mot de passe"
        id="password"
        type="password"
        onChange={(e) => setUser({ ...user, password: e.target.value })}
      />

      <div className="d-flex justify-content-between mx-3 mb-4">
        <MDBCheckbox
          name="flexCheck"
          value=""
          id="flexCheckDefault"
          label="Remember me"
        />
      </div>
      {auth.loginStatus === "rejected" ? (
        <p className="text-danger">{auth.loginError}</p>
      ) : null}
      <button className="btn-login btn-warning mb-4" onClick={handleSubmit}>
        {auth.loginStatus === "pending" ? "Connexion..." : "Se Connecter"}
      </button>

      <div className="text-center">
        <p>
          Vous n'etes pas membre?
          <Link to="/authentification/register">Inscription</Link>
        </p>
      </div>
    </MDBContainer>
  );
}

export default Login;
