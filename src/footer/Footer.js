import React from "react";
import "./footer.css";
import {
  MDBFooter,
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBIcon,
} from "mdb-react-ui-kit";
import FacebookIcon from "@mui/icons-material/Facebook";
import InstagramIcon from "@mui/icons-material/Instagram";
import GoogleIcon from "@mui/icons-material/Google";

import logo from "../assets/logo.png";

const Footer = () => {
  return (
    <MDBFooter className="footer text-center text-lg-start text-muted">
      <section className="logo-footer text-center mt-3 p-3">
        <img src={logo} alt="" />
      </section>

      <section className="">
        <MDBContainer className="text-center text-md-start mt-5">
          <MDBRow className="mt-3">
            <MDBCol md="3" lg="4" xl="3" className="mx-auto mb-4">
              <p>
                <strong> La cuisine de qualité, à portée de main.</strong>
              </p>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </section>
      <section className="d-flex justify-content-center p-4 ">
        <div className="me-5 d-none d-lg-block">
          <span>Suivez-nous sur nos reseaux sociaux:</span>
        </div>

        <div>
          <a href="" className="me-4 text-reset">
            <FacebookIcon />
          </a>
          <a href="" className="me-4 text-reset">
            <InstagramIcon fab icon="twitter" />
          </a>
          <a href="" className="me-4 text-reset">
            <GoogleIcon fab icon="google" />
          </a>

          <a href="" className="me-4 text-reset">
            <MDBIcon fab icon="linkedin" />
          </a>
        </div>
      </section>

      <div
        className="text-center p-4"
        style={{ backgroundColor: "rgba(0, 0, 0, 0.05)" }}
      >
        © 2023 Copyright:
        <a className="text-reset fw-bold" href="https://dicat-dev.vercel.app/">
          DICAT-DEV
        </a>
      </div>
    </MDBFooter>
  );
};

export default Footer;
