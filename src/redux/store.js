import { configureStore } from "@reduxjs/toolkit";
import panierSlice from "./reducers/panierSlice";
import { getTotals } from "./reducers/panierSlice";
import formSlice from "./reducers/formSlice";
import authSlice, { loadUser } from "./reducers/authSlice";

export const store = configureStore({
  reducer: {
    panier: panierSlice,
    form: formSlice,
    auth: authSlice,
  },
});
store.dispatch(getTotals());
store.dispatch(loadUser(null));
