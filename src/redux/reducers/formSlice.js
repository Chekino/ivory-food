import { createSlice } from "@reduxjs/toolkit";
import { toast } from "react-toastify";

const initialState = {
  forms: [],
  form: {},
};

export const formSlice = createSlice({
  name: "form",
  initialState,
  reducers: {
    add: (state, action) => {
      state.forms.push(action.payload);
    },
    find: (state, action) => {
      state.form = state.forms.find((value) => value.id === action.payload);
    },

    update: (state, action) => {
      state.forms.map((form) => {
        if (form.id === action.payload.id) {
          form.name = action.payload.name;
          form.number = action.payload.number;
          form.message = action.payload.message;

          return form;
        }
        return form;
      });
    },
  },
});

// Action creators are generated for each case reducer function
export const { add, find, update } = formSlice.actions;

export default formSlice.reducer;
