import { createSlice } from "@reduxjs/toolkit";
import { toast } from "react-toastify";

const initialState = {
  panier: localStorage.getItem("panier")
    ? JSON.parse(localStorage.getItem("panier"))
    : [],
  panierTotalQuantity: 0,
  panierTotalAmount: 0,
};

export const panierSlice = createSlice({
  name: "panier",
  initialState,
  reducers: {
    addToPanier: (state, action) => {
      //prend l'index du produit dans le panier
      const itemIndex = state.panier.findIndex(
        (item) => item.id === action.payload.id
      );

      //si le produit existe dans le panier : incrémentation +=1 sur panierQuantity
      if (itemIndex >= 0) {
        state.panier[itemIndex].panierQuantity += 1;
        toast.info(
          `Augmentation de la quantité de ${state.panier[itemIndex].name} au panier `,
          {
            position: "bottom-left",
          }
        );
        //si le produit n'existe pas dans le panier ca renvoie ca simplement
      } else {
        const tempProduct = { ...action.payload, panierQuantity: 1 };
        state.panier.push(tempProduct);
        toast.success(`${action.payload.name} ajouté au panier`, {
          position: "bottom-left",
        });
      }
      localStorage.setItem("panier", JSON.stringify(state.panier));
    },
    removeFromPanier: (state, action) => {
      const nextPanier = state.panier.filter(
        (value) => value.id !== action.payload.id
      );

      state.panier = nextPanier;
      localStorage.setItem("panier", JSON.stringify(state.panier));
      toast.error(`${action.payload.name} supprimé du panier`, {
        position: "bottom-left",
      });
    },

    decreasePanier: (state, action) => {
      const itemIndex = state.panier.findIndex(
        (item) => item.id === action.payload.id
      );
      if (state.panier[itemIndex].panierQuantity > 1) {
        state.panier[itemIndex].panierQuantity -= 1;
        toast.info(`${action.payload.name} diminué de la quantité du panier`, {
          position: "bottom-left",
        });
      } else if (state.panier[itemIndex].panierQuantity === 1) {
        const nextPanier = state.panier.filter(
          (value) => value.id !== action.payload.id
        );

        state.panier = nextPanier;

        toast.error(`${action.payload.name} supprimé du panier`, {
          position: "bottom-left",
        });
      }
      localStorage.setItem("panier", JSON.stringify(state.panier));
    },

    clearPanier(state, action) {
      state.panier = [];
      toast.error(`Panier supprimé`, {
        position: "bottom-left",
      });
      localStorage.setItem("panier", JSON.stringify(state.panier));
    },
    getTotals(state, action) {
      let { total, quantity } = state.panier.reduce(
        (panierTotal, panie) => {
          const { price, panierQuantity } = panie;
          const itemTotal = price * panierQuantity;

          panierTotal.total += itemTotal;
          panierTotal.quantity += panierQuantity;

          return panierTotal;
        },
        {
          total: 0,
          quantity: 0,
        }
      );
      state.panierTotalQuantity = quantity;
      state.panierTotalAmount = total;
    },

    /*
    find: (state, action) => {
      state.menu = state.menus.find((value) => value.id == action.payload);
    },
    
    update: (state, action) => {
      state.menus.map((menu) => {
        if (menu.id == action.payload.id) {
          menu.name = action.payload.name;
          menu.description = action.payload.description;
          menu.quantity = action.payload.quantity;
          menu.price = action.payload.price;
          return menu;
        }
        return menu;
      });
    },*/
  },
});

// Action creators are generated for each case reducer function
export const {
  addToPanier,
  removeFromPanier,
  decreasePanier,
  clearPanier,
  getTotals,
} = panierSlice.actions;

export default panierSlice.reducer;
