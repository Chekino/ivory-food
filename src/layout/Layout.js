import React from "react";
import { Route, Routes } from "react-router";
import Accueil from "../pages/accueil/Accueil";
import Menu from "../pages/menu/Menu";
import Contact from "../pages/contact/Contact";
import Header from "../header/Header";
import Footer from "../footer/Footer";
import Panier from "../pages/panier/Panier";

import Register from "../pages/auth/Register";
import Login from "../pages/auth/Login";

const Layout = () => {
  return (
    <div>
      <Header />

      <Routes>
        <Route path="/" element={<Accueil />} />
        <Route path="/menu" element={<Menu />} />
        <Route path="/contact" element={<Contact />} />
        <Route path="/panier" element={<Panier />} />
        <Route path="/authentification" element={<Login />} />
        <Route path="/authentification/register" element={<Register />} />
      </Routes>

      <Footer />
    </div>
  );
};

export default Layout;
