import image1 from "../assets/pates-spaghetti-aux-crevettes.jpg";
import image2 from "../assets/saucisses-feu-camp.jpg";
import image3 from "../assets/steak-juteux-boeuf-saignant-epices-legumes-grilles.jpg";
import image4 from "../assets/kebab-brochettes-tomate.jpg";
import image5 from "../assets/cuisses-poulet-grillees-gril-flamboyant.jpg";
import image6 from "../assets/delicieux-kebab-au-citron-ketchup.jpg";
import image7 from "../assets/steak-cotes-poulet-frit-.jpg";

export const MenuList = [
  {
    name: "Spaghetti aux crevettes",

    id: "1ded",
    price: "9000",
    image: image1,
    category: "Pates",
  },
  {
    name: "saucisse grillé Au feu de bois",

    id: "kdj",
    price: "4000",
    image: image2,
    category: "Saucisses",
  },
  {
    name: "Steak de boeuf legume grillé",

    id: "ifu",
    price: "12000",
    image: image3,
    category: "Steak",
  },
  {
    name: "kebab brochette tomate fraiche",

    id: "kdp",
    price: "5000",
    image: image4,
    category: "Kebab",
  },
  {
    name: "Cuisse de poulet grillé au feu de bois",

    id: "dox",
    price: "5000",
    image: image5,
    category: "Poulet",
  },
  {
    name: "kebab au citron",

    id: "1ed",
    price: "8000",
    image: image6,
    category: "Kebab",
  },
  {
    name: "plateau royal",

    id: "dk8",
    price: "25000 ",
    image: image7,
    category: "Poulet",
  },
];
